# CHANGELOG

Tous les changements notables seront indiqués dans ce fichier.

## Non déployé
- Filtrage par tags
- Moteur de recherche
- Affichage dynamique des posts sur la page d'accueil

## 2.0 - 24/12/2020
### Ajouté
- Gestion des posts avec une BDD (ajout)
- Catégories de posts en dur dans le code source
- Système de tags en lecture seule
- Refonte complète du design
- Section "à propos"
- Système de commentaire avec modération en amont
- Messagerie système pour l'administrateur
