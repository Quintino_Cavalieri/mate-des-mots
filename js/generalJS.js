document.addEventListener("DOMContentLoaded",() => {
	//Gestion de la bannière
	const header=document.getElementsByTagName("header")[0];
	let prevScrollPos=window.pageYOffset;
	window.addEventListener('scroll',() => {
		let currentScrollPos = window.pageYOffset;
		if (prevScrollPos > currentScrollPos) {
			header.style.top="0px";
		}
		else {
			header.style.top="-210px";
		}
		prevScrollPos=currentScrollPos;
	});
	//Envoi d'un commentaire
	const addComment=document.getElementById('addComment');
	if(addComment!=null) {
		addComment.addEventListener('submit', function(event) {
			event.preventDefault();
			loadDocPost("index.php?a=addComment", alert, alert, addComment);
		});
	}
});
//Envoie requête POST à l'adresse <url> avec les données du formulaire <form_id> et callback <cFunction>. En cas d'erreur, exécute <cError> sur la réponse.
function loadDocPost(url, cFunction, cError, form_id) {
	const data=new FormData(addComment);
	const ajx=new XMLHttpRequest();
	ajx.onreadystatechange = function() {
		if(this.readyState==4 && this.status==200) {
			cFunction((this.responseText));
			form_id.reset();
		}
		else if(this.readyState==4 && this.status!=200) {
			cError((this.responseText));
		}
	};
	ajx.open("POST" ,url,true);
	ajx.send(data);
}