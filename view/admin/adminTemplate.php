<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
		<title><?= $title ?></title>
		<meta name="author" content="Kévin Planolles" />
		<meta name="description" content="Recueil de textes" />
		<meta name="robots" content="noindex, nofollow" />
		<link rel="stylesheet" href="../css/admin.css" />
		<link rel="icon" type="image/png" href="../rscr/general/favicon.png" />
	</head>
	<body>
		<header>
			<h1>Zone d'administration</h1>
		</header>
		<nav>
			<a href="index.php">Accueil</a>
		</nav>
		<main>
			<?= $content ?>
		</main>
	</body>
</html>