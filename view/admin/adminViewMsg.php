<?php 
$title="Liste des messages";
ob_start();
?>
<h2>Messages reçus</h2>
<?php
foreach($listMsg as $msg) {
	?>
	<article class="msg">
	<h3><?= $msg['subject'] ?>, le <?= $msg['date'] ?></h3>
	<p><?= $msg['content'] ?></p>
	<p style="width: 40%;"><a href="mailto:<?=$msg['mail'] ?>">Répondre</a></p>
	<p style="width: 40%; text-align: right;"><a href="index.php?action=delMsg&id=<?= $msg['id'] ?>">Supprimer ce message</a></p>
	</article>
	<?php
}
$content=ob_get_clean();
require('adminTemplate.php');