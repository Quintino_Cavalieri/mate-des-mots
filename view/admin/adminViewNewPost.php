<?php
$title="Ajouter un nouveau billet";
ob_start();
?>
<article>
	<h2>Nouveau billet</h2>
	<form enctype="multipart/form-data" autocomplete="off" action="index.php?action=processNewPost" method="post">
		<label for="title">Titre</label><input type="text" name="title" id="title" required="required"/><br />
		<label for="credits">Crédits</label><input type="text" name="credits" id="credits" /><br />
		<label for="category">Catégorie</label><br />
		<select name="category" required="required" id="category">
			<option value="textes">Texte</option>
			<option value="chroniques">Note culturelle</option>
			<option value="billets">Billet de blog</option>
		</select><br />
		<label for="tags">Tags <small>(séparés par des ';')</small></label><br />
		<input type="text" id="tags" name="tags" />
		<label for="description">Description pour la page d'accueil</label><br />
		<input type="text" name="description" id="description" />
		<label for="keywords">Mots-clés pour les moteurs de recherche <small>(séparés par des ',')</small></label><br />
		<input type="text" name="keywords" id="keywords" />
		<label for="date">Date</label><br />
		<input type="date" name="date" id="date" /><br />
		<label for="content">Contenu</label><br />
		<textarea id="content" name="content"></textarea>
		<label for="picture">Image de présentation</label><br />
		<input type="file" id="picture" name="picture" /><br />
		<label for="pdf_file">Fichier pdf (textes uniquement)</label><br />
		<input type="file" id="pdf_file" name="pdf_file" />
		<input type="submit"/>
	</form>
</article>
<?php
$content=ob_get_clean();
require('adminTemplate.php');
?>