<?php
$meta_description="Site web, blog et carnet numérique personnel de Kévin Planolles";
$meta_keywords="";
$title ="À propos - Mate Des Mots";
ob_start();
	?>
	<section>
				<article style="text-align: left; width: 80%;">
					<h2>À propos</h2>
					<h3>À propos de l'auteur</h3>
					<img src="rscr/general/profil_pic5.JPG" alt="Kévin Planolles" style="border-radius: 50%; float: right; max-width: 100%; height: auto;"/>
					<p>Il me semble que la meilleure façon de me présenter est de répondre aux questions que l'on me pose le plus souvent lorsque l'on me rencontre.</p>
					<h4>Comment tu t'appelles ?</h4>
					<p>Kévin Planolles.</p>
					<h4>Quel âge as tu ?</h4>
					<p>27 ans.</p>
					<h4>Tu fais quoi dans la vie ?</h4>
					<p>Je suis actuellement doctorant en écologie. Mon travail est de mettre au point des modèles statistiques ayant pour but d'étudier la biodiversité des poissons côtiers de la Méditerrannée à partir d'images captées grâce à des caméras sous-marines. Je suis également titulaire d'un Master en mathématiques discrètes et appliquées. Si cette partie là de ma vie vous intéresse, je vous redirige vers <a target="_blank" href="rscr/general/matroid_game_theory.pdf">mon mémoire de master.</a></p>
					<h4>Et l'autre moitié du temps ?</h4>
					<p>Je pratique le théâtre en amateur, ce qui inclut d'apprendre des textes, de les jouer voire de les mettre en scène, la plupart du temps au <a rel="external" target="_blank" href="http://www.taust.org">Théâtre Amateur de l'Université des Sciences et Techniques</a>, basé à Montpellier.</p>
					<h4>Et la troisième moitié du temps ?</h4>
					<p>Je la passe à programmer des sites web comme celui-ci en prenant bien soin de réinventer la roue à chaque fois. La quatrième moitié du temps (j'anticipe la question suivante) est occupée à remplir les dits sites webs d'élucubrations diverses et variées, généralement sur l'art et la science. Accessoirement, j'écris des textes à portée artistique.</p>
					<h4>T'écoutes quoi comme musique ?</h4>
					<p>Principalement ce qui est regroupé dans le terme vague "rock". Pour être plus précis, mes artistes préférés incluent : </p><ul><li>les Beatles (y compris les carrières solos d'Harrison, Lennon et McCartney)</li><li> les Rolling Stones</li><li>Serge Gainsbourg</li><li>Queen</li><li>Pink Floyd</li><li>Electric Light Orchestra</li><li>Fleetwood Mac</li><li>Stevie Wonder</li><li>Michel Berger</li><li>Michael Jackson</li><li>Guns N' Roses</li><li>Amy Whinehouse</li><li>Angus & Julia Stone</li><li>Arcade Fire</li><li>Gorillaz (et globalement toute l'oeuvre de Damon Albarn)</li><li>Manu Chao</li><li>Ratatat</li></ul>
					<h3>À propos de ce site</h3>
					<p>Ce site est actuellement en version 2.0. Vous pouvez jeter un oeil à la version 1.0 <a rel="external" target="_blank" href="https://web.archive.org/web/20201224162415/https://mate-des-mots.yn.fr/">ici</a> ou encore <a rel="external" target="_blank" href="https://web.archive.org/web/20201224163523/https://mate-des-mots.yn.fr/textes/argent.php">ici</a> pour constater les changements.<br />
					Cependant, le principe n'a pas changé : il s'agit toujours d'une sorte de carnet numérique pour y mettre mes créations (généralement textuelles) ou tout autre contenu écrit dont j'envisage le partage.</p>
				</article>
			</section>
<?php
$content=ob_get_clean();
require('publicTemplate.php');
?>
