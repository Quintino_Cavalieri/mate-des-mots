<?php 
$meta_description="Site web, blog et carnet numérique personnel de Kévin Planolles";
$meta_keywords="";
$title="Mate des mots";
ob_start();
?>
<section class="last">
<h2><?=$last["title"]?></h2>
<img src="rscr/img/min/<?=$last["url_picture"]?>" alt="<?=$last["title"]?>" />
<?=$preview?>
</section>
<p class="text-center"><em><?=$cat_desc?></em></p>
<section id="menu">
<?php foreach($listPosts as $l) {
	?>
	<a href="publication-<?=$l["id"] . '-' . url_adaptater($l["title"]) ?>" class="card">
		<img src="rscr/img/min/<?=$l["url_picture"]?>" alt="<?=$l["title"]?>" />
		<div class="card-content">
			<h3><?=$l["title"]?></h3>
			<p><?=$l["description"]?></p>
		</div>
		<div class="bottom"><p class="btn">Lire</p></div>
	</a>
<?php
} ?>
</section>
<section>
<p class="text-center">
	<?php 
		if($number > 1) {
			echo '<a href="' . $cat . '1">&lt;&lt;</a> <a href="'. $cat . ($number-1) .'">&lt;</a> ';
		}
		echo 'Page ' . $number. ' sur ' . $nbPages . ' ';
		if ($number < $nbPages) {
			echo '<a href="' . $cat . ($number+1) . '">&gt;</a> <a href="' . $cat . $nbPages .'">&gt;&gt;</a></p>';
		}
	?>
	</section>
	<h2>
	<form action="index.php?a=goToCat" method="post">
	<select id="choice" name="choice">
		<option value="all">Voir tout</option>
		<?php foreach(CATEGORY as $v) {
			if($cat==$v . '/') {
				echo '<option value="' . $v . '" selected>' . ucfirst($v) . '</option>';
			}
			else {
				echo '<option value="' . $v . '">' . ucfirst($v) . '</option>';
			}
		} ?>
	</select>
	<input type="submit" value="Aller" />
	</form>
</h2>
</section>
<?php
		$content=ob_get_clean();
		require("publicTemplate.php");