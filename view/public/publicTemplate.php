<!DOCTYPE html>
<html lang="fr">
	<head>
		<base href="https://www.mate-des-mots.com/" />
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="author" content="Kévin Planolles" />
		<meta name="description" content="<?=$meta_description ?>" />
		<meta name="keywords" content="mate des mots, littérature, théâtre, kévin, planolles, kévin planolles, textes, poésie, blog, poèmes<?=$meta_keywords ?>" />
		<meta property="og:image" content="https://mate-des-mots.com/rscr/general/og_img5.png" />
		<link rel="stylesheet" href="css/public.css" />
		<link rel="icon" type="image/png" href="rscr/general/favicon.png" />
		<script src="js/generalJS.js"></script>
		<title><?=$title ?></title>
	</head>
	<body>
		<header>
			<a href="">
				<h1>Mate des mots</h1><img src="rscr/general/banniere.png" alt="banniere" />
			</a>
			<nav>
				<a href="">Accueil</a>
				<a href="about">À propos</a>
			</nav>
		</header>
		<main>
			<?=$content ?>
		</main>
		<footer>
			<h2>Mate des mots</h2>
			<p>Site développé et maintenu par <em>Kévin Planolles (<a href="mailto:kevin.planolles@protonmail.com">courriel</a>)</em> <br />
			Crédits bannière : <em>Marie C. (@paraknops)</em></p>
			<h3>Les copains</h3>
			<p>
				<a href="http://taust.org/">Le TAUST</a> | <a href="https://www.victorgenieys.com/">Victor Genieys</a> | <a href="https://lesfleursdutemps.wixsite.com/monsite">Les Fleurs du Temps</a>
			</p>
		</footer>
	</body>
</html>
			