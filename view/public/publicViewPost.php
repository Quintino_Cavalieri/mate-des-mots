<?php
ob_start();
?>
<div class="post_card">
	<img src="rscr/img/<?=$post['url_picture']?>" />
	<div class="title">
		<h2><?=$post['title']?></h2>
		<p>
			<strong><?=CAT_TITLE[$post['category']]?></strong><br />
			<em><?=date_to_fr($post['date']) ?></em>
		</p>
	</div>
</div>
<section>
<article><?=$post['content']?>
<?php
if($post['category']=="textes") {
	?>
<h3>Licence</h3>
<p class="text-center">Texte écrit par : <strong><?=$post["credits"]?></strong><br />
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Cette œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.</p>
</p>
<div class="pdf">
<img src="rscr/general/pdf_icon.svg" />
<p class="text-center">&nbsp;Ce texte est disponible au <a target="_blank" href="<?=$post["pdf_file"]?>">format PDF</a></p></div>
<?php
}
else {
	?>
<p class="text-center">Rédigé par <strong><?=$post["credits"]?></strong></p>
<?php
}
if($tags) {
echo "<p class=\"tag-container\">";
foreach($tags as $t) {
	echo '<span class="tag">' . $t["tag"] . '</span> ';
}
echo "</p>";
}
?>
</article>
</section>
<hr />
<section>
<h2>Commentaires</h2>
<?php foreach($comments as $c) {
	?>
	<div class="comment-box">
		<div class="comment-header">
			<h3><?=$c['pseudo']?></h3>
			<h3><?=date('\L\e d/m/Y \à H\hi', strtotime($c["date"]))?></h3>
		</div>
		<p><?=$c['content']?></p>
	</div>
	<?php
}
?>
<form id="addComment">
	<fieldset>
		<legend>Laisser un commentaire</legend>
		<label for="pseudo">Pseudo:</label><br />
		<input type="text" name="pseudo" id="pseudo" /><br />
		<label for="comment">Commentaire: </label><br />
		<textarea id="comment" name="comment"></textarea><br />
		<label for="captcha"><strong>[CAPTCHA] </strong>Quel est le <?=$captcha[1] . $suffixe?> mot du titre de cette publication ?</label><br />
		<input type="text" name="captcha" id="captcha" placeholder="Quel est le <?=$captcha[1] . $suffixe?> mot du titre de cette publication ?" required /><br />
		<p class="text-center"><small><em>Note : les apostrophes font partie d'un mot. Dans <b>"Jeux d'argent"</b>, <b>"d'argent" </b>compte comme un mot. Les "-" comptent comme des espaces et séparent donc deux mots.</em></small></p>
		<input type="hidden" name="post_id" value="<?=$id?>" />
		<input type="submit" />
	</fieldset>
</form>
</section>
<hr />
<section>
	<h2>Dans la même catégorie</h2>
	<div class="see_more">
	<?php foreach($relativePosts as $rp) {
		echo '<a href="publication-' . $rp["id"] . '-' . url_adaptater($rp["title"]) . '" class="mini-card">
			<img src="rscr/img/min/' . $rp["url_picture"] . '" />
			<h3>' . $rp["title"] . '</h3>
			</a>';
	}
	?>
	</div>
</section>
<?php
$content=ob_get_clean();
require('publicTemplate.php');
?>