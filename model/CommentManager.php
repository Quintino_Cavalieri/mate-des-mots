<?php
require_once('Manager.php');
class CommentManager extends Manager {
//	#####LECTURE#####
	public function getCommentsForPost($post_id) {
		$bdd=$this->db_connect();
		$request="SELECT pseudo, content, date FROM commentaires WHERE post_id=:post_id AND visible=1";
		$tmp=$bdd->prepare($request);
		$tmp->bindParam(':post_id', $post_id, PDO::PARAM_INT);
		$tmp->execute();
		return $tmp->fetchAll(PDO::FETCH_ASSOC);
	}
	public function insertCommentToPost($post_id, $pseudo, $content) {
		$bdd=$this->db_connect();
		$request="INSERT INTO commentaires(pseudo, content, post_id) VALUES(:pseudo, :content, :post_id)";
		$tmp=$bdd->prepare($request);
		$tmp->bindParam(':post_id', $post_id, PDO::PARAM_INT);
		$tmp->bindParam(':pseudo', $pseudo, PDO::PARAM_STR);
		$tmp->bindParam(':content', $content, PDO::PARAM_STR);
		$tmp->execute();
	}
	public function deleteCommentById($id) {
		$bdd=$this->db_connect();
		$request="DELETE FROM commentaires WHERE id=:id";
		$tmp=$bdd->prepare($request);
		$tmp->bindParam(':id',$id,PDO::PARAM_INT);
		$tmp->execute();
	}
	public function getLast() {
		$bdd=$this->db_connect();
		$request="SELECT MAX(id) AS last FROM commentaires";
		$tmp=$bdd->prepare($request);
		$tmp->execute();
		return $tmp->fetch(PDO::FETCH_ASSOC);
	}
	public function makeVisible($id) {
		$bdd=$this->db_connect();
		$request="UPDATE commentaires SET visible=1 WHERE id=:id";
		$tmp=$bdd->prepare($request);
		$tmp->bindParam(':id',$id,PDO::PARAM_INT);
		$tmp->execute();
	}
}