<?php 
require_once('Manager.php');
class TagManager extends Manager {
	public function exists_in_db($tag) {
		$bdd=$this->db_connect();
		$request='SELECT id FROM tags WHERE tag="' . $tag . '"';
		$rqt=$bdd->query($request);
		return $rqt->fetch();
	}
	public function insert_in_db($tag) {
		$bdd=$this->db_connect();
		$request='INSERT INTO tags (tag) VALUES ("' . $tag . '")';
		$rqt=$bdd->exec($request);
	}
	public function link_to_post($tag) {
		$bdd=$this->db_connect();
		$rqtPost="SELECT MAX(id) FROM posts";
		$rsltPost=$bdd->query($rqtPost);
		$rsltTag=$bdd->query('SELECT id FROM tags WHERE tag="' . $tag . '"');
		$dtPost=$rsltPost->fetch(PDO::FETCH_ASSOC);
		$dtTag=$rsltTag->fetch(PDO::FETCH_ASSOC);
		$rqtLiaison='INSERT INTO posts_tags (post_id, tag_id) VALUES (' . $dtPost["MAX(id)"] . ', ' . $dtTag["id"] . ')';
		$bdd->exec($rqtLiaison);
	}
	public function getTagsForPost($post_id) {
		$bdd=$this->db_connect();
		$request="SELECT t.tag FROM posts_tags pt INNER JOIN tags t ON pt.tag_id=t.id WHERE pt.post_id=:post_id";
		$tmp=$bdd->prepare($request);
		$tmp->bindParam(':post_id',$post_id,PDO::PARAM_INT);
		$tmp->execute();
		$tags=$tmp->fetchAll(PDO::FETCH_ASSOC);
		return $tags;
	}
}