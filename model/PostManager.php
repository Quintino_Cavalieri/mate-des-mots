<?php 
require_once('Manager.php');
class PostManager extends Manager {
// ######ECRITURE###### 
	public function writePost($title, $credits, $category, $description, $keywords, $date, $content, $img, $pdf) {
		$bdd=$this->db_connect();
		//Traitement de l'image
		if($img['size'] < 1) {
			$url_picture="default.png";
		}
		else {
			$dir='../rscr/img/';
			$filename= str_replace(' ','',time() . basename($img['name']));
			move_uploaded_file($img['tmp_name'], $dir . $filename);
			$url_picture=$filename;
			create_mini($dir . $filename, $filename);
		}
		//Traitement du pdf si nécessaire
		if($category=="textes") {
			if($pdf['size'] > 0) {
				$dir='../rscr/pdf/';
				$filename=str_replace(' ','',$dir . time() . basename($pdf['name']));
				move_uploaded_file($pdf['tmp_name'],$filename);
				$pdf_file=$filename;
			}
			else {
				$pdf_file=null;
			}
		}
		else {
			$pdf_file=null;
		}
		//Traitement du texte
		$content=nl2br($content);
		$content=preg_replace(BBCODE,HTML_TAGS,$content);
		//Insertion du message
		$requete="INSERT INTO posts (title, credits, category, url_picture, description, keywords, date, content, pdf_file) VALUES (:title, :credits, :category, :url_picture, :description, :keywords, :date, :content, :pdf_file)";
		$stmt=$bdd->prepare($requete);
		$stmt->execute(array(
			"title"=>$title,
			"credits"=>$credits,
			"category"=>$category,
			"url_picture"=>$url_picture,
			"description"=>$description,
			"keywords"=>$keywords,
			"date"=>$date,
			"content"=>$content,
			"pdf_file"=>$pdf_file
		));
	}
// ######LECTURE######
	public function getPageByNumber($number) { //Récupère les 8 billets de la page $number. La page 1 est la page d'accueil.
		$numeroPage=(8*$number-7);
		$requete='SELECT id, title, description, url_picture FROM posts ORDER BY date DESC LIMIT :numeroPage , 8';
		$bdd=$this->db_connect();
		$tmp=$bdd->prepare($requete);
		$tmp->bindParam(':numeroPage', $numeroPage, PDO::PARAM_INT);
		$tmp->execute();
		return $tmp->fetchAll(PDO::FETCH_ASSOC);
	}
	public function getLast() { //Renvoie le dernier billet entré
		$requete='SELECT id, title, description, url_picture, content FROM posts ORDER BY date DESC LIMIT 1';
		$bdd=$this->db_connect();
		$tmp=$bdd->query($requete);
		return $tmp->fetch(PDO::FETCH_ASSOC);
	}
	public function getNbPages() { //Renvoie le nombre de pages à raison de 8 billets par pages.
		$requete='SELECT COUNT(id) FROM posts';
		$bdd=$this->db_connect();
		$tmp=$bdd->query($requete);
		$nbBillets=$tmp->fetch(PDO::FETCH_NUM);
		return ceil($nbBillets[0]/8);
	}
	public function getPageByNumberWithCat($number, $cat) { //Récupère les 8 billets de la page $number.
		$numeroPage=(8*($number-1));
		$requete='SELECT id, title, description, url_picture FROM posts WHERE category=:cat ORDER BY date DESC LIMIT :numeroPage,8';
		$bdd=$this->db_connect();
		$tmp=$bdd->prepare($requete);
		$tmp->bindParam(':numeroPage', $numeroPage, PDO::PARAM_INT);
		$tmp->bindParam(':cat', $cat, PDO::PARAM_STR);
		$tmp->execute();
		return $tmp->fetchAll(PDO::FETCH_ASSOC);
	}
	public function getNbPagesWithCat($cat) { //Renvoie le nombre de pages à raison de 8 billets par pages.
		$requete='SELECT COUNT(id) FROM posts WHERE category=:cat';
		$bdd=$this->db_connect();
		$tmp=$bdd->prepare($requete);
		$tmp->bindParam(':cat', $cat, PDO::PARAM_STR);
		$tmp->execute();
		$nbBillets=$tmp->fetch(PDO::FETCH_NUM);
		return ceil($nbBillets[0]/8);
	}
	public function getPostById($id) { //Renvoie le post d'id $id
		$requete='SELECT title, content, credits, date, url_picture, category, description, keywords, pdf_file FROM posts WHERE id=:id';
		$bdd=$this->db_connect();
		$tmp=$bdd->prepare($requete);
		$tmp->bindParam(':id', $id, PDO::PARAM_INT);
		$tmp->execute();
		return $tmp->fetch(PDO::FETCH_ASSOC);
	}
	public function getRelativePosts($category, $itself) { //Récupère les posts de la même catégorie sans le post lui-même
		$requete='SELECT id, title, url_picture FROM posts WHERE category=:category AND id <> :itself ORDER BY RAND() LIMIT 3';
		$bdd=$this->db_connect();
		$tmp=$bdd->prepare($requete);
		$tmp->bindParam(':category',$category,PDO::PARAM_STR);
		$tmp->bindParam(':itself',$itself,PDO::PARAM_INT);
		$tmp->execute();
		return $tmp->fetchAll(PDO::FETCH_ASSOC);
	}
	public function getTitleById($id) {
		$requete='SELECT title FROM posts WHERE id=:id';
		$bdd=$this->db_connect();
		$tmp=$bdd->prepare($requete);
		$tmp->bindParam(':id',$id,PDO::PARAM_INT);
		$tmp->execute();
		return $tmp->fetch(PDO::FETCH_ASSOC);
	}
}
