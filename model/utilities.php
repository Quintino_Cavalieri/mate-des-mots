<?php
// ####### CONSTANTES #######
//Catégories [clé: nom public; valeur: nom bdd]
define ('CATEGORY', array(
	"textes",
	"chroniques",
	"billets"));
//Description des catégories [clé: nom; valeur: description]
define ('CAT_DESC', array(
	"textes"=>"Les <strong>textes</strong> regroupent indifféremment toutes mes expérimentations littéraires, quelque soit leur genre (poésie, théâtre, etc.).",
	"chroniques"=> "Les <strong>chroniques</strong> sont des retours personnels sur des oeuvres culturelles.",
	"billets"=>"Les <strong>billets</strong> regroupent des réflexions personnelles sur divers sujets (souvent en rapport avec les mathématiques ou la littérautre)."));
define ('CAT_TITLE', array (
	"textes"=>"Texte littéraire",
	"chroniques"=>"Chronique",
	"billets"=>"Billet"));
//Balises BBcode et équivalents html en regex
define ('BBCODE', array(
	"#\|\|(.*?)\|\|#",
	"#\[g\](.*?)\[/g\]#",
	"#\[e\](.*?)\[/e\]#",
	"#\[titre=([3456])\](.*?)\[/titre\]#",
	"#\[url=(.+)\](.*?)\[/url\]#",
	"#\[cite\](.*?)\[/cite\]#",
	"#\[gif=(.+)\]#",
	"#\[i\](.*?)\[/i\]#",
	"#\[u\](.*?)\[/u\]#"));
define ('HTML_TAGS', array(
	"<p>$1</p>",
	"<strong>$1</strong>",
	"<em>$1</em>",
	"<h$1>$2</h$1>",
	'<a target="_blank" href="$1">$2</a>',
	"<blockquote>$1</blockquote>",
	'<div style="text-align: center;"><img src="$1"/></div>',
	'<i>$1</i>',
	'<u>$1</u>'));
// ####### FONCTIONS UTILITAIRES #######
	function create_mini($img, $filename) { //Fonction qui créé la miniature d'une image donnée par $img et la sauvegarde sous le nom $filename
		$source=imagecreatefromjpeg($img);
		$width_source=imagesx($source);
		$height_source=imagesy($source);
		$mini=imagecreatetruecolor((int)($width_source*600/$height_source),600);
		$width_mini=imagesx($mini);
		$height_mini=imagesy($mini);
		imagecopyresampled($mini, $source,0,0,0,0,$width_mini,$height_mini,$width_source,$height_source);
		imagejpeg($mini, "../rscr/img/min/" . $filename, 95);
	}
function strip_special_chars($date) { //Enlève les caractères spéciaux d'une chaîne de caractères
	$unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
                            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
                            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
                            'ö'=>'o', 'œ'=>'oe', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', '?'=>'','!'=>'', ':'=>'', '.'=>'.',','=>' ');
	$date = strtr($date, $unwanted_array );
	return $date;
}
function url_adaptater($date) { //Adapte une chaine de caractères pour une url : enlève les accents, apostrophes, majuscules et remplace les espaces par des '-'
	$to_strip = array('\'',' ','"');
	return strtolower(str_replace($to_strip, '-', rtrim(strip_special_chars($date))));
}
function date_to_fr($date) { //Parce que la fonction par défaut est craquée...
	$tmpDate=explode('-', $date);
	$year=$tmpDate[0];
	$month=$tmpDate[1];
	$day=$tmpDate[2];
	$months=array('01'=>'Janvier', '02'=>'Février', '03'=>'Mars','04'=>'Avril','05'=>'Mai','06'=>'Juin','07'=>'Juillet','08'=>'Août','09'=>'Septembre','10'=>'Octobre','11'=>'Novembre',
		'12'=>'Décembre');
	$month=$months[$month];
	$day=ltrim($day,'0');
	if($day=='1') {
		$day='1er';
	}
	return $day . ' '. $month . ' ' . $year;
}
function generate_captcha($title) { //Renvoie un tableau dont la première entrée est le mot et la seconde sa position.
	$title=str_replace('-', ' ', strtolower(rtrim((strip_special_chars($title)))));
	$words=explode(' ', $title);
	$pos=strlen($title) % count($words);
	return array($words[$pos], $pos+1);
}