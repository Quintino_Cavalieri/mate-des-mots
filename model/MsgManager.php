<?php 
require_once('Manager.php');

class MsgManager extends Manager {
	public function getMessages() {
		$bdd=$this->db_connect();
		$tmp=$bdd->query('SELECT id, subject, mail, date, content FROM messages');
		$listMsg=$tmp->fetchAll(PDO::FETCH_ASSOC);
		return $listMsg;
	}
	public function delMessageById($id) {
		$bdd=$this->db_connect();
		$prep=$bdd->exec('DELETE FROM messages WHERE id=' . $id);
	}
	public function insertMessage($subject, $mail, $content) {
		$bdd=$this->db_connect();
		$requete="INSERT INTO messages(subject,mail,date,content) VALUES(:subject,:mail,NOW(),:content)";
		$tmp=$bdd->prepare($requete);
		$tmp->execute(array(
			"subject"=>$subject,
			"mail" => $mail,
			"content" => $content));
	}
		
}