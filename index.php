<?php 
setlocale(LC_ALL, ["fr", "fra","fr_FR"]);
$host  = $_SERVER['HTTP_HOST'];
$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
require_once('model/PostManager.php');
require_once('model/utilities.php');
require_once('model/TagManager.php');
require_once('model/CommentManager.php');
require_once('model/MsgManager.php');
require_once('controller/publicController.php');
try {
	if(isset($_GET['a'])) {
		switch($_GET['a']) {
			case "about":
				viewAbout();
			break;
			case "addComment":
				writeComment($_POST['pseudo'],$_POST['comment'],$_POST['captcha'],$_POST['post_id']);
			break;
			case "goToCat":
				$goto=htmlspecialchars($_POST['choice']);
				if($goto=="all") {
					header('Location: https://' . $host . $uri);
				}
				else {
					header('Location: https://' . $host . $uri . '/' . $goto . '/1');
				}
			break;
			case "menu":
				if(isset($_GET['p']) && (int)$_GET['p']>1) {
					viewIndex($_GET['p']);
					}
				else {
					http_response_code(303);
					header('Location:https://' . $host . $uri);
				}
			break;
			case "menuByCat":
				if(isset($_GET['c'])) {
					if(isset($_GET['p'])) {
						viewIndexByCat($_GET['c'], $_GET['p']);
					}
					else {
						http_response_code(303);
						header('Location:https://' . $host . $uri . '/' . $_GET['c'] . '/1');
					}
				}
				else {
					throw new Exception('Catégorie non renseignée',1);
				}
			break;
			case "publi":
				if(!isset($_GET['id'])) {
					throw new Exception('Identifiant non accessible',1);
				}
				else {
					if(isset($_GET['query']) && urlFitTitle($_GET['id'], $_GET['query'])) {
						viewPost($_GET['id']);
					}
					else {
						throw new Exception('Mauvaise combinaison des paramètres de la publication',1);
					}
				}
			break;
			default:
			throw new Exception('Action non reconnue',1);
			break;
		}
	}
	else {
		viewIndex(1);
	}
}
catch(Exception $e) {
	/* GESTION DES EXCEPTIONS
	Les codes en vigueur pour les exceptions sont actuellement les suivants : 
	1 : Erreur sur le paramètre (http code : 400)
	2 : Ressource inaccessible (http code : 404) */
	switch($e->getCode()) {
		case 1 : 
			http_response_code(400);
		break;
		case 2 : 
			http_response_code(404);
		break;
		default:
			http_response_code(418);
		break;
	}
	echo 'Erreur: ' . $e->getMessage();
}

    
