<?php
require_once('../model/adminModel.php');
require_once('../model/PostManager.php');
require_once('../model/utilities.php');
require_once('../model/TagManager.php');
require_once('../model/CommentManager.php');
require_once('../model/MsgManager.php');
require_once('../controller/adminController.php');
try {
	if(isset($_GET['action'])) {
		switch($_GET['action']) {
			case "checkLogin":
				if(isset($_POST['pass'])) {
					checkLogin($_POST['pass']);
				}
				else {
					throw new Exception('Pas de mot de passe entré');
				}
				break;
			case "login":
				viewLogin();
				break;
			case "newPost":
				viewNewPost();
				break;
			case "processNewPost":
		addPost($_POST['title'], $_POST['credits'], $_POST['category'],$_POST['tags'],$_POST['description'], $_POST['keywords'], $_POST['date'],$_POST['content'],$_FILES['picture'],$_FILES['pdf_file']);
				break;
			case "viewMsg":
				viewMsg();
				break;
			case "delMsg":
				deleteMessageById($_GET['id']);
				break;
			case "deleteComment":
				deleteCommentById($_GET["id"]);
			break;
			case "acceptComment":
				acceptComment($_GET['id']);
			break;
			default:
				viewIndex();
		}
	}
	else {
		viewIndex();
	}
}
catch(Exception $e) {
	echo 'Erreur: ' . $e->getMessage();
}