<?php
setlocale(LC_TIME,"fr_FR");
//Cette fonction ajoute un texte dans la BDD.
function addPost($title, $credits, $category, $tags, $description, $keywords, $date, $content, $img, $pdf) {
	if(isAdmin()) {
		//On appelle le PostManager pour ajouter le post en BDD
		$pMng = new PostManager;
		$pMng->writePost($title, $credits, $category, $description, $keywords, $date, $content, $img, $pdf);
		//Si le tag existe on l'associe au post sinon on l'ajoute préalablement
		if(strlen($tags) > 0) {
			$Tmng = new TagManager;
			$listTag=explode(';',$tags);
			foreach($listTag as $t) {
				if ($Tmng->exists_in_db($t)) {
					$Tmng->link_to_post($t);
				}
				else {
					$Tmng->insert_in_db($t);
					$Tmng->link_to_post($t);
				}
			}
		}
		header('Location: index.php');
	}
	else {
		viewLogin();
	}
}
//Cette fonction affiche la page d'identificaition administrateur. 
function viewLogin() { 
	require('../view/admin/adminViewLogin.php');
}
//Cette fonction vérifie si l'utilisateur est un administrateur et le redirige vers la connexion sinon.
function isAdmin() {
	if(empty($_SESSION['is_admin'])) {
		return false;
	}
	else {
		return true;
	}
}
//Cette fonction effectue la vérification du mot de passe administrateur.
function checkLogin($pass) {
	if(connectAdmin($pass)) {
		require('../view/admin/adminViewIndex.php');
	}
	else {
		viewLogin();
	}
}
//Cette fonction affiche la page d'ajout d'un nouveau post.
function viewNewPost() {
	if(isAdmin()) {
		require('../view/admin/adminViewNewPost.php');
	}
	else {
		viewLogin();
	}
}
// Cette fonction affiche le panneau d'administration.
function viewIndex() {
	if(isAdmin()) {
		require('../view/admin/adminViewIndex.php');
	}
	else {
		viewLogin();
	}
}
// Cette fonction récupère la liste des messages à l'administrateur.
function viewMsg() {
	if(isAdmin()) {
		$mMng=new MsgManager;
		$listMsg=$mMng->getMessages();
		require('../view/admin/adminViewMsg.php');
	}
	else {
		viewLogin();
	}
}
//Cette fonction supprime dans la BDD un message identifié par son id.
function deleteMessageById($id) {
	if(isAdmin()) {
		$mMng=new MsgManager;
		$mMng->delMessageById($id);
		header('Location: index.php?action=viewMsg');
	}
	else {
		viewLogin();
	}
}
//Cette fonction supprime un commentaire identifié par $id
function deleteCommentById($id) {
	if(isAdmin()) {
		$cMng=new CommentManager;
		$cMng->deleteCommentById($id);
		header('Location: index.php?action=viewMsg');
		echo "coucou";
	}
	else {
		viewLogin();
	}
}
// Cette fonction rend visible un commentaire identifié par son $id
function acceptComment($id) {
	if(isAdmin()) {
		$cMng=new CommentManager;
		$cMng->makeVisible($id);
		header('Location: index.php?action=viewMsg');
	}
	else {
		viewLogin();
	}
}