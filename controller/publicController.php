<?php 
function viewAbout() { //Affiche à propos
	require('view/public/publicViewAbout.php');
}
function viewIndex($number) { //Cette fonction affiche l'accueil à la page $number
	$pMng=new PostManager;
	$last=$pMng->getLast();
	if(!$last) {
		throw new Exception("La dernière publication n'a pas pu être récupérée",2);
	}
	//Génération du texte d'aperçu
	$link='<p class="text-center"><a href="publication-' . $last["id"] . '-' . url_adaptater($last["title"]) .'">Lire la suite…</a></p>';
	$preview=strstr($last["content"], '[cut]',true) . '--><br />' . $link;
	//Neutralisation de $number et récupération des 8 messages de la page idoine
	$nbPages=$pMng->getNbPages();
	$number=(int) $number;
	if($number <= 0 or $number > $nbPages) {
		throw new Exception(' paramètre de page hors limite',1);
	}
	$listPosts=$pMng->getPageByNumber($number);
	if(!$listPosts) {
		throw new Exception('Impossible de récupérer la page' . $number,2);
	}
	$cat="";
	$cat_desc="Toutes les publications sur <strong>Mate des mots</strong>";
	require('view/public/publicViewIndex.php');
}
function viewIndexByCat($cat, $number){ //Cette fonction affiche sur l'accueil les billets de catégorie $cat à la page $number
	$pMng=new PostManager;
	$last=$pMng->getLast();
	if(!$last) {
		throw new Exception("Le dernier post n'a pas pu être récupéré",2);
	}
	//Génération du texte d'aperçu
	$link='<p class="text-center"><a href="publication-' . $last["id"] . '-' . url_adaptater($last["title"]) .'">Lire la suite…</a></p>';
	$preview=strstr($last["content"], '[cut]',true) . '--><br />' . $link;
	//Neutralisation de $cat et $number et récupération des 8 messages de la page idoine
	if(!in_array($cat, CATEGORY)) {
		throw new Exception(' catégorie non existante');
	}
	$number=(int) $number;
	$nbPages=$pMng->getNbPagesWithCat($cat);
	if($number <= 0 or $number > $nbPages) {
		throw new Exception(' paramètre de page hors limites',1);
	}
	$listPosts=$pMng->getPageByNumberWithCat($number,$cat);
	if(!$listPosts) {
		throw new Exception('Impossible de récupérer la page' . $number,2);
	}
	$cat_desc=CAT_DESC[$cat];
	$cat=$cat . '/';
	require('view/public/publicViewIndex.php');
}
function viewPost($id) { //Cette fonction affiche le post d'id $id
	//Post
	$pMng=new PostManager;
	$id=(int) $id;
	$post=$pMng->getPostById($id);
	if(!$post) {
		throw new Exception('Impossible de récupérer le post n°' . $id,2);
	}
	$meta_description=strip_tags($post['description']);
	$meta_keywords='';
	$title=$post['title'] . ' - Mate Des Mots';
	if(strlen($post['keywords']) > 0) {
		$meta_keywords.=',' . $post['keywords'];
	}
	//Tags
	$tMng=new TagManager;
	$tags=$tMng->getTagsForPost($id);
	$relativePosts=$pMng->getRelativePosts($post["category"], $id);
	//Commentaires
	$cMng=new CommentManager();
	$comments=$cMng->getCommentsForPost($id);
	$captcha=generate_captcha($post["title"]);
	if($captcha[1]==1) {
		$suffixe='er';
	}
	else {
		$suffixe='ème';
	}
	require('view/public/publicViewPost.php');
}
function urlFitTitle($id, $url_title) { //Retourne vrai si l'URL correspond à l'id et non sinon
	$id=(int)$id;
	$pMng=new PostManager;
	$title=$pMng->getTitleById($id)["title"];
	if(!$title) {
		throw new Exception('Impossible de récupérer le titre du post n°' . $id,2);
	}
	else {
		if(url_adaptater($title)==$url_title) {
			return true;
		}
		else  {
			return false;
		}
	}
}
function writeComment($pseudo, $content, $captcha, $post_id) { //Vérification d'un commentaire utilisateur et notification de l'administrateur
	$captcha=strtolower(rtrim(strip_special_chars($captcha)));
	$pMng=new PostManager;
	$cMng=new CommentManager;
	$mMng=new MsgManager;
	$title=$pMng->getTitleById($post_id)["title"];
	$word=generate_captcha($title)[0];
	//Validation
	if(!empty($pseudo) && !empty($content) && !empty($captcha) && !empty($post_id) && $captcha==$word) {
		$cMng->insertCommentToPost($post_id, $pseudo, $content);
		$last=$cMng->getLast()['last'];
		$subject='Nouveau commentaire de ' . $pseudo . ' sur la publication <em>' . $title . '</em>';
		$mail='Message système';
		$content='<p>Voici le message : </p><blockquote>' . $content . '</blockquote><p><a href="index.php?action=deleteComment&id=' . $last . '">Supprimer</a><br />
			<a href="index.php?action=acceptComment&id=' . $last . '">Valider</a></p>';
		$mMng->insertMessage($subject, $mail, $content);
		echo "Commentaire envoyé à l'auteur ! Il sera publié ou non à la discrétion de celui-ci.";
		http_response_code(200);
	}
	else if($captcha!=$word) {
		echo "La validation par captcha a échoué. N'hésitez pas à copier/coller le mot demandé. Si l'erreur persiste, contactez l'auteur. Attention, si vous êtes un robot, je ne peux rien pour vous.";
		http_response_code(400);
	}
	else {
		echo "L'envoi du commentaire a echoué pour cause de formulaire mal rempli. Si l'erreur persiste, contactez l'auteur.";
		http_response_code(400);
	}
}